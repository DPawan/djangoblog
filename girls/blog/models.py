from django.db import models
from django.conf import settings
from django.utils import timezone
from multiselectfield import MultiSelectField

# Create your models here.
tag_choices = (('news', 'news'),
        ('entertainment', 'entertainment'),
        ('knowledge', 'knowledge'),
        ('story', 'story'))


class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    tags = MultiSelectField(choices=tag_choices, default='story')
    def publish(self):
        self.published_date = timezone.now()
        self.save()
    
    def __str__(self):
        return self.title
